from django import forms
from datetime import datetime, date, timezone

from .models import Event

class ScheduleForm(forms.Form):
    CATEGORY_CHOICES = (
        ('AC', 'Academic'),
        ('I', 'Important'),
        ('O', 'Other'),
        ('H', 'Holiday')
    )
    name = forms.CharField(max_length=30)
    place = forms.CharField(max_length=60)
    date = forms.DateField(label="Date (YYYY-MM-DD)")
    time = forms.TimeField(label="Time (HH:mm)")
    category = forms.ChoiceField(choices=CATEGORY_CHOICES, label="Category", initial='', widget=forms.Select())