from django.shortcuts import render, HttpResponseRedirect
from .forms import ScheduleForm
from .models import Event

# Create your views here.
def index(request):
    query_results = Event.objects.all()
    response = {'query_results': query_results}
    return render(request, 'schedule.html', response)

def form(request):
    form = ScheduleForm()
    return render(request, 'form.html', {'form': form})

def post(request):
    form = ScheduleForm(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response = {}
        response['name'] = request.POST['name']
        response['place'] = request.POST['place']
        response['date'] = request.POST['date']
        response['time'] = request.POST['time']
        response['category'] = request.POST['category']
        event = Event(name=response['name'], place=response['place'], date=response['date'], time=response['time'], category=response['category'])

        event.save()
        return index(request)
    else:
        return form(request)

def deleteAll(request):
    Event.objects.all().delete()
    response = {}
    return index(request)