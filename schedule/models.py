from django.db import models
from datetime import datetime, date

# Create your models here.
class Event(models.Model):
    CATEGORY_CHOICES = (
        ('AC', 'Academic'),
        ('I', 'Important'),
        ('O', 'Other'),
        ('H', 'Holiday')
    )
    name = models.CharField(max_length=30)
    place = models.CharField(max_length=60)
    date = models.DateField()
    time = models.TimeField()
    category = models.CharField(max_length=2, choices=CATEGORY_CHOICES)

    def __str__(self):
        return self.name