from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='sch_index'),
    path('form/', views.form, name='sch_form'),
    path('form/add', views.post, name='add_schedule'),
    path('delete-all', views.deleteAll, name='delete_all'),
]
