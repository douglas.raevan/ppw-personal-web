from django.test import TestCase, Client
from django.urls import resolve
from .views import index, work_experiences

# Create your tests here.
class ProfileUnitTest(TestCase):

	def setUp(self):
		self.client = Client();

	def test_homepage_url(self):
		page = self.client.get('')
		self.assertTrue(page.status_code, 200)

	def test_homepage_template(self):
		page = self.client.get('')
		self.assertTemplateUsed(page, 'index.html')

	def test_homepage_using_index_func(self):
		found = resolve('/')
		self.assertEqual(found.func, index)

	def test_workexperiences_url(self):
		page = self.client.get('/work-and-experiences')
		self.assertTrue(page.status_code, 200)

	def test_workexperiences_template(self):
		page = self.client.get('/work-and-experiences')
		self.assertTemplateUsed(page, 'work-and-experiences.html')

	def test_workexperiences_using_work_experiences_func(self):
		found = resolve('/work-and-experiences')
		self.assertEqual(found.func, work_experiences)