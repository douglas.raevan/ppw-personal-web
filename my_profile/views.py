from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def index(request):
    response = {}
    return render(request, 'index.html', response)

def work_experiences(request):
    response = {}
    return render(request, 'work-and-experiences.html', response)