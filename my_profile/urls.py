from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    #path('home', views.index, name='index'),
    path('work-and-experiences', views.work_experiences, name='work-and-experiences')
]
