from django import forms

from .models import User

class UserRegistration(forms.ModelForm):
    
    class Meta:
        model = User
        fields = ('email', 'name', 'password')
        widgets = {
            'password': forms.PasswordInput(),
        }