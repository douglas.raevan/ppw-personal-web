from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.core import serializers
import json

from .forms import UserRegistration
from .models import User

# Create your views here.
def registration_page(request):
	form = UserRegistration()
	return render(request, 'register.html', {'form': form})

def user_register(request):
	if (request.method == 'POST'):
		form = UserRegistration(request.POST)
		if form.is_valid():
			response = {}
			response['email'] = request.POST['email']
			response['name'] = request.POST['name']
			response['password'] = request.POST['password']
			user = User(email=response['email'], name=response['name'], password=response['password'])

			user.save()
			return registration_page(request)
	return registration_page(request)

def get_user_object(request, id):
	try:
		obj = User.objects.get(pk=id)
		data = {'email': id};
		my_json = json.dumps(data)
		return HttpResponse(my_json, content_type='application/json')
	except:
		my_json = json.dumps({'email': None})
		return HttpResponse(my_json, content_type='application/json')

