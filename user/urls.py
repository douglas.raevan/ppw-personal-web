from django.urls import path
from . import views

urlpatterns = [
    path('', views.registration_page, name='reg_form'),
    path('user_register/', views.user_register, name='user_register'),
    path('get_user/<str:id>/', views.get_user_object, name='get_user_object')
]
