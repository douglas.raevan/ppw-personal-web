from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='rw_index'),
]
