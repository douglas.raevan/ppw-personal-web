from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('books_data', views.data, name='books_data')
]
