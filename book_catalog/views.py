import json
import requests
from django.shortcuts import render
from django.http import JsonResponse, HttpResponse

def index(request):
	

	'''
	for i in data:
		print(i['volumeInfo']['title'])'''

	response = {}
	return render(request, 'book-catalog.html', response)

def data(request):
	query = request.GET['query']
	file = requests.get("https://www.googleapis.com/books/v1/volumes?q="+query).json()
	my_str = json.dumps(file)
	
	return HttpResponse(my_str, content_type="application/json")